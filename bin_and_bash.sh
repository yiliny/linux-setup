#!/bin/bash
source "`dirname $0`/global_constants.sh"

# Get my executables
git clone "${YILINYGIT}/bin.git"

# Setup my .bashrc
git clone "${YILINYGIT}/bashrc.git"
bashrc/install.sh
source .bashrc #adds ~/bin to PATH
