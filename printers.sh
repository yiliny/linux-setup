#!/bin/bash

# EFFECTS:  Installs some printer driver packages for the Brother HL-2140.

source "`dirname $0`/global_constants.sh"

DRIVER_PACKAGE_DIR=$DIR/brother_printer_drivers

sudo dpkg -i --force-all $DRIVER_PACKAGE_DIR/brhl2140lpr-2.0.2-1.i386.deb
sudo dpkg -i --force-all $DRIVER_PACKAGE_DIR/cupswrapperHL2140-2.0.2-1.i386.deb

echo "Go into the HL-2140's printer settings, uninstall the old printer, and switch the active printer to using Foomatic/HL1250 as its PPD."
