#!/bin/bash

# EFFECTS:  Install lemonade clipboard tool for SSH.
# REQUIRES: Preexisting `~/.local/bin` folder, which is in PATH.

source "`dirname $0`/global_constants.sh"

ARCHIVE=lemonade.tar.gz
URL=https://github.com/pocke/lemonade/releases/download/v1.1.1/lemonade_linux_amd64.tar.gz
BINARY=lemonade

curl -fLo $ARCHIVE --create-dirs $URL
tar -xzf $ARCHIVE
chmod +x $BINARY
mv $BINARY ~/.local/bin
