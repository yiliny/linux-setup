#!/bin/bash

# REQUIRES: Active python installation. `habitica` executable won't be accessible
#           unless `/home/$USER/.local/bin` is added to PATH.
# EFFECTS:  Installs `philadams/habitica` command-line tool.
# DETAIL:   habitica --help         # for detailed help documentation

source "`dirname $0`/global_constants.sh"

pip install --user habitica

CONFIG_FOLDERS=".config .config/habitica"
CONFIG=".config/habitica/auth.cfg"

for folder in $CONFIG_FOLDERS; do
    if [ ! -d $folder ]; then
        mkdir $folder
    fi

    if [ ! -f $CONFIG ]; then
        touch $CONFIG
    fi

cat << EOF > $CONFIG
[Habitica]
url = https://habitica.com
login = a0d32e21-9089-4c58-b090-6a9e3c38163d
password = c0fafceb-e506-476a-a546-65fc6d80444a

EOF

    chmod 600 ~/.config/habitica/auth.cfg

done
