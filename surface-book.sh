#!/bin/bash

YILINYGIT="https://yiliny@bitbucket.org/yiliny"
INSTALLCMD="sudo apt-get install -y"

# Start from Home Folder
cd ~/

# Clone the Firmware Folder
git clone "${YILINYGIT}/itouch.git"

sudo mkdir /itouch
sudo cp ~/itouch/* /itouch
sudo /itouch/symlink.sh

# Get linux header images from here:
# mega.nz/#!KQ4CmQCR!vYcyTWD-KWDuYhnZ6cFsw6eq1XSnXvjgR-S64MQKsDU
