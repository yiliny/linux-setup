#!/bin/bash
source "`dirname $0`/global_constants.sh"

# Install Slic3r
$INSTALLCMD slic3r

# Install Meshlab 3D mesh viewer
$INSTALLCMD meshlab

# Build OctoPrint from source and install
$INSTALLCMD python-pip python-dev python-setuptools python-virtualenv git libyaml-dev build-essential
git clone https://github.com/foosel/OctoPrint.git $DIR/OctoPrint
virtualenv venv
#./venv/bin/pip install pip --upgrade
sudo python $DIR/OctoPrint/setup.py install

mkdir ~/.octoprint # for config files
