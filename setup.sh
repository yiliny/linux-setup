#!/bin/bash
source "`dirname $0`/global_constants.sh"

sudo echo "You now have root!"

# Without this, running *immediately* after installation (for Ubuntu WSL)
# will result in apt failing with 404 when retrieving packages.
sudo apt update

$DIR/dependencies.sh    # Install dependencies for the installation itself
$DIR/clang.sh           # Install clang, and clangd language server.
#$DIR/ubuntu_native.sh   # Ubuntu-specific setup options
$DIR/ssh_advanced.sh    # SSH option configuration
$DIR/git_basic.sh       # Basic Git installation, git package, name, email, etc.
$DIR/bin_and_bash.sh    # Clone my bin folder and source my bashrc settings.
$DIR/pdfgrep.sh         # Install pdfgrep tool.
$DIR/bear.sh            # Install compile_commands.json generator tool.
$DIR/vim_install.sh     # Set up vim the way I like it.
$DIR/tmux_setup.sh      # Set up tmux the way I like it.
#$DIR/alacritty.sh       # Set up my favorite TTY.
$DIR/git_advanced.sh    # Set up diffconflicts, neovim as core editor.
$DIR/security.sh        # Set up GPG key generator.
$DIR/profiling.sh       # Install perf profiler

# Reminders of other things that I need to do
echo "Automated setup complete."
echo "If you run into issues with syntax errors in grub, make sure there's an EOF character in the modified Ubuntu logo!"
