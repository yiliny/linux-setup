#!/bin/bash

# REQUIRES: SSH is installed and keys have been copied over.
# EFFECTS:  Creates an SSH alias for CAEN. Installs xauth and xorg, which
#           may or may not be necessary for successful X forwarding.
# DETAIL:   More information on setting up X forwarding can be found at
#           the following links:
#               https://stackoverflow.com/a/46356959
#               https://unix.stackexchange.com/a/57143

source "`dirname $0`/global_constants.sh"

# Install xorg and xauth.
$INSTALLCMD xorg xauth

# Create an alias that'll let me log into CAEN
if [ ! -f .ssh/config ]; then
    git clone $YILINYGIT/ssh.git
    ~/ssh/install.sh
fi
