#!/bin/bash
source "`dirname $0`/global_constants.sh"

## Install git ##
$INSTALLCMD git

# Git: Configure Email, Name, and Push Behavior
git config --global user.email "yiliny@umich.edu"
git config --global user.name "Yilin Yang"
git config --global push.default simple
