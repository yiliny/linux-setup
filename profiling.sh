#!/bin/bash

# EFFECTS:  Installs perf tools onto the current system.

source "`dirname $0`/global_constants.sh"

$INSTALLCMD linux-tools-generic
