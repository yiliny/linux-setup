#!/bin/bash

# REQUIRES: Git is already installed.
# EFFECTS:  Compiles and installs the driver for the Realtek RTL8821AU USB
#           Wi-Fi dongle.
# DETAILS:  Taken from the following link:
#               https://medium.com/@at15/ubuntu-16-04-8811au-wireless-driver-d29d6929a50c

source "`dirname $0`/global_constants.sh"

$INSTALLCMD dkms

cd /tmp
git clone https://github.com/abperiasamy/rtl8812AU_8821AU_linux.git
cd rtl8812AU_8821AU_linux
make
sudo make install
sudo modprobe rtl8812au
