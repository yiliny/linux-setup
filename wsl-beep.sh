#!/bin/bash

# EFFECTS:  Clones, compiles, and installs mobluse/wsl-beep.

source "`dirname $0`/global_constants.sh"

cd /tmp
git clone https://github.com/mobluse/wsl-beep
cd wsl-beep
make
sudo make install
