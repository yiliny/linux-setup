#!/bin/bash

# EFFECTS:  Install sysstat tools, which contains sar. Enables sysstat logging.

source "`dirname $0`/global_constants.sh"

$INSTALLCMD sysstat

# Enable logging.
sudo cat << 'EOF' > /etc/default/sysstat
#
# Default settings for /etc/init.d/sysstat, /etc/cron.d/sysstat
# and /etc/cron.daily/sysstat files
#

# Should sadc collect system activity informations? Valid values
# are "true" and "false". Please do not put other values, they
# will be overwritten by debconf!
ENABLED="true"

EOF
