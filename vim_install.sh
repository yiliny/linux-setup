#!/bin/bash

# REQUIRES: Git is installed.
# EFFECTS:  Clones my vimrc folder, sets up vim the way I like it.

source "`dirname $0`/global_constants.sh"

# Install Vim and Neovim
git clone "${YILINYGITHUB}/vimrc.git"
vimrc/install.sh
