#!/bin/bash

# EFFECTS: Installs node.js.

source "`dirname $0`/global_constants.sh"

MAJOR_VERSION=18

curl -sL https://deb.nodesource.com/setup_${MAJOR_VERSION}.x | sudo -E bash -
sudo apt update
$INSTALLCMD nodejs
