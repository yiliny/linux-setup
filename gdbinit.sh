#!/bin/bash

# REQUIRES: Git is installed.
# EFFECTS:  Clones my GDBinit folder, sets up GDB the way I like it.

source "`dirname $0`/global_constants.sh"

git clone "${YILINYGIT}/gdbinit.git"
gdbinit/install.sh
