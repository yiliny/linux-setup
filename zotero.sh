#!/bin/bash
# EFFECTS:  Installs standalone instance of Zotero.

source "`dirname $0`/global_constants.sh"

sudo apt-add-repository ppa:smathot/cogscinl
sudo apt-get update
$INSTALLCMD zotero-standalone
