#!/bin/bash

# EFFECTS:  Configure fail2ban such that it *permanently* bans IP addresses after
#           too many failed attempts.

CONFIG="iptables-multiport.conf"
DEST_DIR="/etc/fail2ban/action.d/"
DEST_FILE=$DEST_DIR/$CONFIG

sudo cp $CONFIG $DEST_DIR
sudo chmod 644 $DEST_FILE
