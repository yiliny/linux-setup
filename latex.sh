#!/bin/bash

# EFFECTS:  Installs a build environment for TeX document files.

source "`dirname $0`/global_constants.sh"

# TexLive is the "standard" TeX distribution for Ubuntu/Debian.
#   This command installs ALL of the TeX build environment. This is a massive
#   installation, but it should guarantee that I always have the right packages
#   installed.
#
#   This installation is around 3.5GB.
#
#   https://help.ubuntu.com/community/LaTeX
$INSTALLCMD texlive-full

# Installs biber, for making citations.
$INSTALLCMD biber
