#!/bin/bash
set -Eeuo pipefail
set -x

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
YILINYGIT="git@bitbucket.org:yiliny"
YILINYGITHUB="git@github.com:Yilin-Yang"
INSTALLCMD="sudo apt-get install -y --fix-missing"

# Start from Home Folder
cd ~/

# trap CTRL-C, have it terminate everything
trap "echo 'Terminated script.' && exit 0" SIGINT
