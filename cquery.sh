#!/bin/bash

# REQUIRES: git, python, clang 3.4 or greater
# EFFECTS:  Installs and builds the cquery language server for C++ into
#           $INSTALLDIR.

source "`dirname $0`/global_constants.sh"

### The installation directory for the cquery executable. ###
INSTALLDIR=~/.local/stow/cquery

cd ~/
if [ ! -d temp ]; then
    mkdir temp
fi
cd temp

if [ ! -d cquery ]; then
    git clone https://github.com/jacobdufault/cquery --recursive
fi
cd cquery

./waf configure --prefix $INSTALLDIR    # --prefix is optional, it specifies
                                        # install directory
./waf build    # -g -O3, built build/release/bin/cquery
./waf install  # optional, copies the executable to $PREFIX/bin/cquery
