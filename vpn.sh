#!/bin/bash

# EFFECTS:  Installs OpenVPN and configures it to use NordVPN.
#           Installs deluge, deluge-console, and deluged (deluge daemon).
source "`dirname $0`/global_constants.sh"


$INSTALLCMD deluge deluge-console deluged

$INSTALLCMD openvpn
cd /etc/openvpn
$INSTALLCMD ca-certificates
sudo wget http://downloads.nordcdn.com/configs/archives/servers/ovpn.zip
$INSTALLCMD unzip
sudo unzip ovpn.zip
sudo rm zip

echo "============================================================"
echo 'List servers using `ls -al` from this directory.'
echo 'Start OpenVPN with a given configuration using `sudo openvpn [filename]`'
echo '      e.g. sudo openvpn de117.nordvpn.com.udp1194.ovpn'
echo 'Disconnect by hitting Ctrl-C in the active terminal.'
