#!/bin/bash

# EFFECTS:  Installs Resilio Sync.

source "`dirname $0`/global_constants.sh"

echo "deb http://linux-packages.resilio.com/resilio-sync/deb resilio-sync non-free" | sudo tee /etc/apt/sources.list.d/resilio-sync.list

wget -qO - https://linux-packages.resilio.com/resilio-sync/key.asc | sudo apt-key add -

sudo apt-get update
$INSTALLCMD resilio-sync

sudo systemctl enable resilio_sync
