#!/bin/bash

# REQUIRES: Git is already installed. Neovim is already installed.
#           ctags is already installed. diffconflicts won't work unless `~/bin`
#           is added to PATH.
# EFFECTS:  Sets up diffconflicts as a mergetool and neovim as my primary text editor.

source "`dirname $0`/global_constants.sh"

# Git: Install Git LFS.
$INSTALLCMD software-properties-common
$INSTALLCMD curl
sudo add-apt-repository ppa:git-core/ppa

curl -L https://packagecloud.io/github/git-lfs/gpgkey | sudo apt-key add -
curl -s https://packagecloud.io/install/repositories/github/git-lfs/script.deb.sh | sudo bash

$INSTALLCMD git-lfs
git lfs install

# Git: Set diffconflicts as mergetool.
git config --global merge.tool diffconflicts
git config --global mergetool.diffconflicts.cmd 'nvim -c DiffConflicts "$MERGED" "$BASE" "$LOCAL" "$REMOTE"'
git config --global mergetool.diffconflicts.trustExitCode true
git config --global mergetool.diffconflicts.keepBackup false

# Git: Set neovim as default text editor.
git config --global core.editor "nvim"
