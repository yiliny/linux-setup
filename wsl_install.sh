#!/bin/bash

# EFFECTS:  Performs setup operations that are specific to WSL, including:
#           -Installing xclip (for proper clipboard support with VcXsrv)
#           -Cloning a wsl-terminal installation, which should be copied to
#           the Windows filesystem for proper installation.

source "`dirname $0`/global_constants.sh"

git clone "${YILINYGIT}/wsl-terminal.git"

$INSTALLCMD xclip
