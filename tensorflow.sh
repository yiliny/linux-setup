#!/bin/bash

# EFFECTS:  Installs tensorflow CPU for Python3.x in the folder
#           ~/machine_learning/tensorflow.

source "`dirname $0`/global_constants.sh"

TARGET_DIR=~/machine_learning/tensorflow

$INSTALLCMD python3-pip python3-dev python-virtualenv
virtualenv --system-site-packages -p python3 $TARGET_DIR
source ~/tensorflow/bin/activate
easy_install -U pip
pip3 install --upgrade tensorflow
exit
