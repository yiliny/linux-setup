#!/bin/bash

# EFFECTS:  Installs TrueType and OpenType monospace fonts.

source "`dirname $0`/global_constants.sh"

if [ ! -d ~/.fonts ]; then
    mkdir ~/.fonts
fi

FONTS_DIR=$DIR/monospace_fonts

FONTS="
    mononoki \
    source-code-pro \
    inconsolata \
"

for FONT in $FONTS; do
    cp -R "$FONTS_DIR/$FONT" ~/.fonts/$FONT
done

fc-cache -f -v
