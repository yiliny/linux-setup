#!/bin/bash
# EFFECTS:  Installs Ubuntu 16.04's build dependencies for Unreal Engine 4.
# DETAIL:   Taken from the following link:
#               https://wiki.unrealengine.com/Building_On_Linux#Installing_Dependencies

source "`dirname $0`/../global_constants.sh"

$INSTALLCMD build-essential \
            mono-mcs \
            mono-devel \
            mono-xbuild \
            mono-dmcs \
            mono-reference-assemblies-4.0 \
            libmono-system-data-datasetextensions4.0-cil \
            libmono-system-web-extensions4.0-cil \
            libmono-system-management4.0-cil \
            libmono-system-xml-linq4.0-cil \
            cmake \
            dos2unix \
            clang-3.5 \
            libfreetype6-dev \
            libgtk-3-dev \
            libmono-microsoft-build-tasks-v4.0-4.0-cil \
            xdg-user-dirs
