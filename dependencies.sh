#!/bin/bash

# REQUIRES: SSH is installed and keys have been copied over.
# EFFECTS:  Fixes the permissions on the SSH keys and other config files.
#           Creates an SSH alias for CAEN.

source "`dirname $0`/global_constants.sh"

GLOBAL_DEPENDENCIES="\
    cmake \
    cmake-curses-gui \
    curl \
    g++ \
    gcc \
    gettext \
    make \
    ninja-build \
    pkg-config \
    python3 \
    python3-pip \

"
$INSTALLCMD $GLOBAL_DEPENDENCIES

# and install Node.js stuff
"$DIR/node.sh"
"$DIR/yarn.sh"
