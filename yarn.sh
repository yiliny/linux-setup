#!/bin/bash

# EFFECTS:  Installs yarn.

sudo mkdir -p /usr/local/share/keyrings/
curl -sL https://dl.yarnpkg.com/debian/pubkey.gpg | gpg --dearmor | sudo tee /usr/local/share/keyrings/yarn.gpg
echo "deb [signed-by=/usr/local/share/keyrings/yarn.gpg] https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
sudo apt-get update && sudo apt-get install yarn
