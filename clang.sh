#!/bin/bash

# EFFECTS:  Install clang, with associated development tools and the LLVM
#           runtime. Crucially, installs clang-tools, with associated clangd,
#           as well as lldb.

source $(dirname "$0")/global_constants.sh


# the clang version to be installed
CLANG_MAJOR_VERSION=14
DISTRO_VERSION=$(lsb_release -c | cut -f 2-)

# Add LLVM repository
# wget -O - https://apt.llvm.org/llvm-snapshot.gpg.key | sudo apt-key add -

# sudo apt-add-repository "deb http://apt.llvm.org/${DISTRO_VERSION}/ llvm-toolchain-${DISTRO_VERSION}-${CLANG_MAJOR_VERSION} main"
# sudo apt-get update
$INSTALLCMD clang*${CLANG_MAJOR_VERSION} clang-tools-${CLANG_MAJOR_VERSION}
$INSTALLCMD clang lldb # "ordinary" clang and LLDB, for remote debugging

sudo update-alternatives --install /usr/bin/clang++ clang++ "/usr/bin/clang++-${CLANG_MAJOR_VERSION}" 100
sudo update-alternatives --install /usr/bin/clang clang "/usr/bin/clang-${CLANG_MAJOR_VERSION}" 100
sudo update-alternatives --install /usr/bin/clangd clangd "/usr/bin/clangd-${CLANG_MAJOR_VERSION}" 100
