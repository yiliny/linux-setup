#!/bin/bash

# EFFECTS:  Installs GIMP and a HIDPI icon theme for the same.

source "`dirname $0`/global_constants.sh"

ARCHIVE=gimp-hidpi-master.zip
TEMPFILE=/tmp/$ARCHIVE

$INSTALLCMD gimp

VERSION=`gimp -v | head -n1 | sed 's/.*\([0-9]\.[0-9]\)\.[0-9].*/\1/'`
GIMPDIR=~/.gimp-$VERSION

curl -fLo $TEMPFILE \
    https://github.com/jedireza/gimp-hidpi/archive/master.zip
unzip $TEMPFILE -d $GIMPDIR/themes
mv $GIMPDIR/themes/gimp-hidpi-master $GIMPDIR/themes/gimp-hidpi
