#!/bin/bash

# REQUIRES: Git is installed.
# EFFECTS:  Clones my tmux folder, sets up tmux the way I like it.

source "`dirname $0`/global_constants.sh"

# Install tmux.
$INSTALLCMD tmux

# Install my config stuff.
git clone "${YILINYGIT}/tmux.git"
tmux/install.sh
