#!/bin/bash

# REQUIRES: GCC is already installed. cmake is already installed.
#           make is already installed. python is already installed.
# EFFECTS:  Installs Build EAR onto the current system.
# DETAIL:   https://github.com/rizsotto/Bear

source "`dirname $0`/global_constants.sh"

$INSTALLCMD bear
