#!/bin/bash

# EFFECTS:  Install pyreverse, a tool for generating UML diagrams from
#           Python code.
# DETAILS:  Usage information taken from:
#               https://twigstechtips.blogspot.com/2014/09/python-visualise-your-class-hierarchy.html

source `dirname $0`/global_constants.sh

$INSTALLCMD graphviz libgraphviz-dev python-dev
pip install --upgrade --user pylint pygraphviz
