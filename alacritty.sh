#!/bin/bash

# REQUIRES: git.
# EFFECTS:  Clones my alacritty repository. Configures alacritty the way
#           I like it.

source "`dirname $0`/global_constants.sh"

git clone "${YILINYGIT}/alacritty.git"
alacritty/install.sh
