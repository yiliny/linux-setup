#!/bin/bash
source "`dirname $0`/global_constants.sh"

UBUNTU_VERSION="`lsb_release -r | awk '{print $2}'`"

# Stop Windows Clock from Borking Due to Linux's Use of UTC
timedatectl set-local-rtc 1

# Install Google Chrome
wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | sudo apt-key add -
sudo sh -c 'echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google-chrome.list'
sudo apt-get update
$INSTALLCMD google-chrome-stable

# Install Spotify
$DIR/spotify.sh

if [ $UBUNTU_VERSION == 18.04 ]; then
    # GNOME is hot flaming garbage
    $INSTALLCMD ubuntu-unity-desktop
    $INSTALLCMD lightdm
    sudo apt remove gdm3
    sudo apt remove gnome-shell
fi

# Install Configuration Tools
$INSTALLCMD gnome-tweak-tool
$INSTALLCMD unity-tweak-tool
$INSTALLCMD compizconfig-settings-manager
$INSTALLCMD compiz-plugins-extra
$INSTALLCMD dconf-editor

# Install Cosmetic Stuff
sudo apt-add-repository ppa:numix/ppa
sleep 2
sudo apt-get update
$INSTALLCMD numix-gtk-theme
$INSTALLCMD numix-icon-theme-circle
$INSTALLCMD numix-wallpaper-*

# Make wallpapers display properly. Taken from:
#   https://superuser.com/a/870995
gsettings set org.gnome.settings-daemon.plugins.background active true

# Install mononoki
$DIR/fonts.sh

# Install Grub-Customizer
sudo add-apt-repository ppa:danielrichter2007/grub-customizer
sudo apt-get update
$INSTALLCMD grub-customizer

# Black Grub Background, Not Magenta
printf 'if background_color 0,0,0; then\n  clear\nfi\n\n' | sudo tee /usr/share/plymouth/themes/ubuntu-logo/ubuntu-logo.grub
$INSTALLCMD compizconfig-settings-manager

# xbindkeys
$INSTALLCMD xbindkeys
git clone "${YILINYGIT}/xbindkeys.git"
xbindkeys/install.sh
xbindkeys
#sudo touch /etc/init.d/start-xbindkeys.sh
#sudo chmod 755 /etc/init.d/start-xbindkeys.sh
#printf '#!/bin/bash\nxbindkeys_autostart' | sudo tee /etc/init.d/start-xbindkeys.sh
#sudo update-rc.d start-xbindkeys.sh defaults

# Install f.lux
sudo add-apt-repository ppa:nathan-renniewaldock/flux
sudo apt-get update
$INSTALLCMD fluxgui

# Install tmux
$INSTALLCMD tmux

### SSH STUFF ###

# Install OpenSSH Server
$INSTALLCMD openssh-server

# Install Mosh
$INSTALLCMD mosh

# Install fail2ban
$INSTALLCMD fail2ban

# Start the SSH server
sudo service ssh start

echo "Copy your id_rsa and id_rsa.pub into .ssh!"
echo "You do not need to fix their permissions or ssh-add them. Those'll be handled from here!"

# Show our IP address
ifconfig

echo "When done, press any key to continue!"
read -n1
