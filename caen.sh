#!/bin/bash

# REQUIRES: The `caen` branches of ~/bashrc and ~/bin have both been cloned and
#           configured.
# EFFECTS:  Sets up all of my stuff on CAEN!

source "`dirname $0`/global_constants.sh"

curl -fsSL https://raw.githubusercontent.com/arxanas/caenbrew/master/install.sh | sh

# Commands:
#
#     caenbrew list: List all packages.
#     caenbrew list -s '<term>': List all packages matching a search term.
#     caenbrew install <package>: Install a package.
#     caenbrew install -f <package>: Force-install or reinstall a package. (Use
#                                    caenbrew install -f caenbrew if you want
#                                    to update Caenbrew.)
#     caenbrew uninstall <package>: Uninstall a package.
#
#     Caenbrew will automatically resolve and install dependencies for you. For
#     example, the tmux package will automatically install libevent and
#     ncurses.

# Install neovim dependencies!
DEPENDENCIES="
    clang \
    llvm \
    pip \
    python \
    python3 \
"
for DEP in $DEPENDENCIES; do
    caenbrew install $DEP
done

# Create a pyvenv if one doesn't already exist. Use that for a global pip3 executable.
pyvenv caen_pyvenv
