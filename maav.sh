#!/bin/bash

# REQUIRES: Git is installed. My SSH private key has been properly configured.
# EFFECTS:  Clones MAAV's work repositories.

source "`dirname $0`/global_constants.sh"

### MAAV STUFF ###

# Install ccmake
$INSTALLCMD cmake-curses-gui

# Clone MAAV Repositories
mkdir maav
git clone git@git.maavumich.org:nav.git maav/nav
git clone git@git.maavumich.org:ctrl.git maav/ctrl

# Create symlinks to appropriate .yvimrc files
ln -s ~/vimrc/.yvimrc-maav ~/maav/.yvimrc

# Clone MAAV Subversion Repositories
$INSTALLCMD subversion

# cd into maav folder
cd ~/maav

# Checkout circuits subversion repo
svn checkout svn://svn.maavumich.org/circuits

# Checkout structures subversion repo
svn checkout svn://svn.maavumich.org/zeus

# Return to home folder
cd ~/
